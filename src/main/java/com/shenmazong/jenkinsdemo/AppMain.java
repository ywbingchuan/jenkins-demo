package com.shenmazong.jenkinsdemo;

/**
 * @author 军哥
 * @version 1.0
 * @description: AppMain
 * @date 2022/9/1 9:38
 */

public class AppMain {

    public static void main(String[] args) {

        while(true) {

            System.out.println("time=ok," + System.currentTimeMillis());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
